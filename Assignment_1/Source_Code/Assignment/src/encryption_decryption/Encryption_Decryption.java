package encryption_decryption;

import javax.swing.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.awt.*;
import java.io.*;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SealedObject;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.imageio.ImageIO;

import java.security.*;

import java.util.Arrays;
import java.util.Base64;
import java.util.Random;

public class Encryption_Decryption {
	
	//RES
	private int bit;
	private KeyPairGenerator kpg;
	private KeyPair myPair;
	private PublicKey publicKey;
	private PrivateKey privateKey;
	//AES
	private String secretKeyAES;
	private static SecretKeySpec secretKey;
    private static byte[] keyAES;
	//DES
	private static SecretKey keyDES = null;
	
	private String algorithm;
	private String data;  //Convert file to string  
	private String nameFile;
	
	private JTextField txtKey;
	private JTextField txtInput;
	private JTextField txtOutput;
	
	//JProgressBar Status
	private static JProgressBar jpb;
	
	
	public void initializeWindow() {
		/*
		 * Frame Encryption Application
		 */
    	JFrame frmApp = new JFrame();
    	frmApp.setTitle("Encryption and Decryption");
    	frmApp.setResizable(false);
    	frmApp.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	frmApp.setSize(700, 550);
    	frmApp.getContentPane().setLayout(null);
    	/*
    	 * Create ComboBox Algorithm
    	 */
    	String Agr[] = {"DES", "AES", "RSA"};
    	JComboBox cbAgr = new JComboBox(Agr);
    	cbAgr.setBounds(175,150,70,35);
    	frmApp.getContentPane().add(cbAgr);
    	cbAgr.addActionListener(new ActionListener() {
    		@Override
    		public void actionPerformed(ActionEvent e) {
        		if (cbAgr.getSelectedIndex() != -1) {
        			if (cbAgr.getItemAt(cbAgr.getSelectedIndex()) == "DES") {
        				algorithm = "DES";
        			}else if (cbAgr.getItemAt(cbAgr.getSelectedIndex()) == "AES") {
        				algorithm = "AES";
        			}
        			else if (cbAgr.getItemAt(cbAgr.getSelectedIndex()) == "RSA") {
        				algorithm = "RSA";
        			}	
        		}
        	}
    	});
    	/*
    	 * Creat button CreateKey
    	 */
        JButton btPKey = new JButton("CreateKey");
        btPKey.setBounds(270, 150, 100, 35);
    	frmApp.getContentPane().add(btPKey);
        btPKey.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent e) {
        	if (algorithm == "DES") {
        		try {
        			keyDES = KeyGenerator.getInstance("DES").generateKey();
				} catch (NoSuchAlgorithmException e2) {
					e2.printStackTrace();
				}
   		        try {
					writeToFile("key\\keyDES.txt", keyDES);
					JOptionPane.showMessageDialog(null,"Tạo Key thành công! \n"+"Lưu trong thư mục key của project");
   		        } catch (Exception e1) {
					e1.printStackTrace();
				}
   			}else if (algorithm == "AES") {
   				//
   			}else if (algorithm == "RSA") {
   				try {
   				// Get an instance of the RSA key generator
					kpg = KeyPairGenerator.getInstance("RSA");
					kpg.initialize(bit);
					myPair = kpg.generateKeyPair();
				} catch (NoSuchAlgorithmException e1) {
					e1.printStackTrace();
				}
   				try {
					writeToFile("key\\publicKeyRSA.txt", myPair.getPublic());
					writeToFile("key\\privateKeyRSA.txt", myPair.getPrivate());
					JOptionPane.showMessageDialog(null,"Tạo publicKey và privateKey thành công! \n"+"Lưu trong thư mục key của project");
				} catch (Exception e1) {
					e1.printStackTrace();
				}
   			}	
           }
        });
        /*
         * Create JComboBox Bits
         */
    	String Bits[] = {"512", "1024", "2048"};
     	JComboBox cbBits = new JComboBox(Bits);
     	cbBits.setBounds(480,150,70,35);
     	frmApp.getContentPane().add(cbBits);
     	cbBits.addActionListener(new ActionListener() {
     		@Override
     		public void actionPerformed(ActionEvent e) {
         		if (cbBits.getSelectedIndex() != -1) {
         			if (cbBits.getItemAt(cbBits.getSelectedIndex()) == "512") {
        				bit = 512;
        			}else if (cbBits.getItemAt(cbBits.getSelectedIndex()) == "1024") {
        				bit = 1024;
        			}else if (cbBits.getItemAt(cbBits.getSelectedIndex()) == "2048") {
        				bit = 2048;
        			}
         		}
         	}
     	});
    	/*
    	 * Create Textfield Key
    	 */
    	txtKey = new JTextField();
    	txtKey.setBounds(175, 215, 250, 35);
    	frmApp.getContentPane().add(txtKey);
    	txtKey.setColumns(10);
    	/*
    	 * Create button Open Key
    	 */
    	JFileChooser  fileDialog3 = new JFileChooser();
        JButton btOpKey = new JButton("Open Key");
        btOpKey.setBounds(460, 215, 90, 35);
    	frmApp.getContentPane().add(btOpKey);
    	btOpKey.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent e) {
              int returnVal = fileDialog3.showOpenDialog(frmApp);
              if (returnVal == JFileChooser.APPROVE_OPTION) {
            	 java.io.File file3 = fileDialog3.getSelectedFile();
            	 txtKey.setText(file3.getPath());
              }
              else {
            	  txtKey.setText("Open command cancelled by user." );           
              }      
           }
        });
    	
    	/*
    	 * Create Textfield Input
    	 */
    	txtInput = new JTextField();
    	txtInput.setBounds(175, 275, 250, 35);
    	frmApp.getContentPane().add(txtInput);
    	txtInput.setColumns(10);
    	/*
    	 * Creat button Open file
    	 */
    	JFileChooser fileDialog = new JFileChooser();
        JButton btOpenfile = new JButton("Open File");
        btOpenfile.setBounds(460, 275, 90, 35);
    	frmApp.getContentPane().add(btOpenfile);
        btOpenfile.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent e) {
              int returnVal = fileDialog.showOpenDialog(frmApp);
              if (returnVal == JFileChooser.APPROVE_OPTION) {
                 java.io.File file = fileDialog.getSelectedFile();
                 txtInput.setText(file.getPath());
                 nameFile = file.getName();
                 //encode File to Base64 Binary
                 try {
  					data = encodeFileToBase64Binary(file);
  				} catch (IOException e1) {
  					e1.printStackTrace();
  				}
              }
              else {
            	  txtInput.setText("Open command cancelled by user." );           
              }      
           }
        });
    	
    	/*
    	 * Create Textfield Output E & D
    	 */
    	txtOutput = new JTextField();
    	txtOutput.setBounds(175, 340, 250, 35);
    	frmApp.getContentPane().add(txtOutput);
    	txtOutput.setColumns(10);
    	/*
    	 * Creat button Browse
    	 */
    	JFileChooser  fileDialog2 = new JFileChooser();
        JButton btBrowse = new JButton("Browse");
        btBrowse.setBounds(460, 340, 90, 35);
    	frmApp.getContentPane().add(btBrowse);
    	btBrowse.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent e) {
              int returnVal = fileDialog2.showOpenDialog(frmApp);
              if (returnVal == JFileChooser.APPROVE_OPTION) {
            	 java.io.File file2 = fileDialog2.getSelectedFile();
                 txtOutput.setText(file2.getParent());
              }
              else {
            	  txtOutput.setText("Open command cancelled by user." );           
              }      
           }
        });
    	/*
    	 * Create button Encrypt
    	 */
    	JButton btEncrypt = new JButton("Encrypt");
    	btEncrypt.setBounds(150, 400, 100, 35);
    	frmApp.getContentPane().add(btEncrypt);
    	btEncrypt.addActionListener(new ActionListener() {
    		@Override
    		public void actionPerformed(ActionEvent e) {
    			//-------------------------------------------------------------------------Encrypt
    			if (algorithm == "DES") {
    				try {
						keyDES = (SecretKey)readFromFile(txtKey.getText());
						jpb.setValue(20);
					} catch (Exception e2) {
						e2.printStackTrace();
					}
    				Cipher cipherDES = null;
					try {
						cipherDES = Cipher.getInstance("DES");
					} catch (NoSuchAlgorithmException | NoSuchPaddingException e1) {
						e1.printStackTrace();
					}
    		        try {
    		        	cipherDES.init(Cipher.ENCRYPT_MODE, keyDES);
    		        	jpb.setValue(50);
					} catch (InvalidKeyException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
    		        String input = data;  //Chuỗi cần mã hóa
    		        SealedObject EncryptedMessageDES = null;
					try {
						EncryptedMessageDES = new SealedObject(input, cipherDES);
						jpb.setValue(70);
					} catch (IllegalBlockSizeException | IOException e1) {
						e1.printStackTrace();
					}//Chuỗi đã được mã hóa
    		        try {
						writeToFile(txtOutput.getText()+"\\"+nameFile, EncryptedMessageDES);
						jpb.setValue(100);
						JOptionPane.showMessageDialog(null,"Mã hóa thành công!");
						jpb.setValue(0);
					} catch (Exception e1) {
						e1.printStackTrace();
					}   //Ghi ra file
    			}else if (algorithm == "AES") {
    				//
    				secretKeyAES = readFiletoString(txtKey.getText());
    				jpb.setValue(20);
    				String encryptedString = encrypt(data, secretKeyAES);
    				jpb.setValue(50);
    				try {
    					converttoFile(encryptedString);
    					jpb.setValue(100);
    					JOptionPane.showMessageDialog(null,"Mã hóa thành công!");
    					jpb.setValue(0);
					} catch (Exception e1) {
						e1.printStackTrace();
					}	 				
    			}else if (algorithm == "RSA") {
    				Cipher cipherRSA = null;
					try {
						cipherRSA = Cipher.getInstance("RSA");
					} catch (NoSuchAlgorithmException | NoSuchPaddingException e1) {
						e1.printStackTrace();
					}
					try {
						publicKey = (PublicKey)readFromFile(txtKey.getText());  // publicKey
						jpb.setValue(40);
					} catch (Exception e1) {
						e1.printStackTrace();
					}
    		        try {
    		        	cipherRSA.init(Cipher.ENCRYPT_MODE, publicKey);
    		        	jpb.setValue(60);
					} catch (InvalidKeyException e1) {
						e1.printStackTrace();
					}
    		        String input = data;  //Chuỗi cần mã hóa
    		        SealedObject EncryptedMessageRSA = null;
					try {
						EncryptedMessageRSA = new SealedObject(input, cipherRSA);
						jpb.setValue(80);
					} catch (IllegalBlockSizeException | IOException e1) {
						e1.printStackTrace();
					}//Chuỗi đã được mã hóa
    		        try {
						writeToFile(txtOutput.getText()+"\\"+nameFile, EncryptedMessageRSA);
						jpb.setValue(100);
						JOptionPane.showMessageDialog(null,"Mã hóa thành công!");
						jpb.setValue(0);
					} catch (Exception e1) {
						e1.printStackTrace();
					}   //Ghi ra file	
    			}	
        	}
    	});
    	/*
    	 * Create button Decrypt
    	 */
    	JButton btDecrypt = new JButton("Decrypt");
    	btDecrypt.setBounds(300, 400, 100, 40);
    	frmApp.getContentPane().add(btDecrypt);
    	btDecrypt.addActionListener(new ActionListener() {
    		@Override
    		public void actionPerformed(ActionEvent e) {
        		//---------------------------------------------------------------------------Decrypt
    			if (algorithm == "DES") {
					try {
						keyDES = (SecretKey)readFromFile(txtKey.getText());
						jpb.setValue(10);
					} catch (Exception e1) {
						e1.printStackTrace();
					}
    			     SealedObject EncryptedMessageDES = null;
					try {
						EncryptedMessageDES = (SealedObject)readFromFile(txtInput.getText());
						jpb.setValue(20);
					} catch (Exception e1) {
						e1.printStackTrace();
					}
    			     String algorithmName = EncryptedMessageDES.getAlgorithm();
    			     Cipher cipherDES = null;
					try {
						cipherDES = Cipher.getInstance(algorithmName);
						jpb.setValue(50);
					} catch (NoSuchAlgorithmException | NoSuchPaddingException e1) {
						e1.printStackTrace();
					}
    			     try {
    			    	 cipherDES.init(Cipher.DECRYPT_MODE, keyDES);
    			    	 jpb.setValue(80);
					} catch (InvalidKeyException e1) {
						e1.printStackTrace();
					}
    			     try {
						String text = (String)EncryptedMessageDES.getObject(cipherDES);
						converttoFile(text);
						jpb.setValue(100);
						JOptionPane.showMessageDialog(null,"Giải mã thành công!");
						jpb.setValue(0);
					} catch (ClassNotFoundException | IllegalBlockSizeException | BadPaddingException
							| IOException e1) {
						e1.printStackTrace();
					}
    			}else if (algorithm == "AES") {
    				//
    				secretKeyAES = readFiletoString(txtKey.getText());
    				jpb.setValue(40);
    				String encryptedString = data;
    				String decryptedString = decrypt(encryptedString, secretKeyAES);
    				jpb.setValue(80);
    				try {
						converttoFile(decryptedString);
						jpb.setValue(100);
						JOptionPane.showMessageDialog(null,"Giải mã thành công!");
						jpb.setValue(0);
					} catch (FileNotFoundException e1) {
						e1.printStackTrace();
					}
    			}else if (algorithm == "RSA") {
    				try {
    					privateKey = (PrivateKey)readFromFile(txtKey.getText());   // privateKey
    					jpb.setValue(20);
					} catch (Exception e1) {
						e1.printStackTrace();
					}
    			     SealedObject EncryptedMessageRSA = null;
					try {
						EncryptedMessageRSA = (SealedObject)readFromFile(txtInput.getText());
						jpb.setValue(50);
					} catch (Exception e1) {
						e1.printStackTrace();
					}
    			     Cipher cipherRSA = null;
					try {
						cipherRSA = Cipher.getInstance("RSA");
					} catch (NoSuchAlgorithmException | NoSuchPaddingException e1) {
						e1.printStackTrace();
					}
    			     try {
    			    	 cipherRSA.init(Cipher.DECRYPT_MODE, privateKey);
    			    	 jpb.setValue(80);
					} catch (InvalidKeyException e1) {
						e1.printStackTrace();
					}
    			     try {
						String message = (String)EncryptedMessageRSA.getObject(cipherRSA);
						converttoFile(message);
						jpb.setValue(100);
						JOptionPane.showMessageDialog(null,"Giải mã thành công!");
						jpb.setValue(0);
					} catch (ClassNotFoundException | IllegalBlockSizeException | BadPaddingException
							| IOException e1) {
						e1.printStackTrace();
					}
    			}
        	}
    	});	   	
    	/*
    	 * Create button Reset
    	 */
    	JButton btReset = new JButton("Reset");
    	btReset.setBounds(450, 400, 100, 40);
    	frmApp.getContentPane().add(btReset);
    	btReset.addActionListener(new ActionListener() {
    		@Override
    		public void actionPerformed(ActionEvent e) {
    			txtKey.setText("");
        		txtInput.setText("");
        		txtOutput.setText("");
        		jpb.setValue(0);
        	}
    	});	 
    	/*
    	 * Create status bar
    	 */
    	jpb=new JProgressBar(0,100);
    	jpb.setBounds(150,460,400,25);
    	jpb.setStringPainted(true);
    	frmApp.getContentPane().add(jpb);
    	
    	/*
    	 * Create background
    	 */
    	JLabel fram = new JLabel();
    	fram.setBounds(0,0,700,550);
    	frmApp.getContentPane().add(fram);
    	try {
			BufferedImage image = ImageIO.read(new File("backpround/background.jpg"));
			ImageIcon icon = new ImageIcon(image.getScaledInstance(700, 550, Image.SCALE_SMOOTH));
			fram.setIcon(icon);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
    	
    	frmApp.setVisible(true);
    }
	/*
	 * Encode File to Base64 Binary
	 */
	public String encodeFileToBase64Binary(File file) throws IOException {
		String encodedfile = null;
		try {
			FileInputStream fileInputStreamReader = new FileInputStream(file);
			byte[] bytes = new byte[(int)file.length()];
			fileInputStreamReader.read(bytes);
			encodedfile = Base64.getEncoder().encodeToString(bytes);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return encodedfile;
	}
    /*
     * Write and read to File
     */
	//DES & RSA
	private static void writeToFile(String filename, Object object) throws Exception {
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(new File(filename)));
        oos.writeObject(object);
        oos.flush();
        oos.close();
    }
	//DES & RSA
    private static Object readFromFile(String filename) throws Exception {
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(new File(filename)));
        Object object = ois.readObject();
        ois.close();
        return object;
    }
    /*
     * Read File to String use BufferedReader
     */
    private static String readFiletoString(String filePath) {
        StringBuilder contentBuilder = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null) {
                contentBuilder.append(sCurrentLine).append("\n");
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return contentBuilder.toString();
    }
    /*
     * Convert Cipher string to File --------------------------------------------
     */
    public void converttoFile(String encode) throws FileNotFoundException {
    	FileOutputStream fos1 = new FileOutputStream(txtOutput.getText()+"\\"+ nameFile);
		try {
			fos1.write(Base64.getDecoder().decode(encode));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		try {
			fos1.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    /*
     *  Algorithm AES
     */
    public static void setKey(String myKey) {
        MessageDigest sha = null;
        try {
            keyAES = myKey.getBytes("UTF-8");
            sha = MessageDigest.getInstance("SHA-1");
            keyAES = sha.digest(keyAES);
            keyAES = Arrays.copyOf(keyAES, 16);
            secretKey = new SecretKeySpec(keyAES, "AES");
        }
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
    public static String encrypt(String strToEncrypt, String secret) {
        try {
            setKey(secret);
            jpb.setValue(20);
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            jpb.setValue(40);
            return Base64.getEncoder().encodeToString(cipher.doFinal(strToEncrypt.getBytes("UTF-8")));
        }
        catch (Exception e) {
            System.out.println("Error while encrypting: " + e.toString());
        }
        return null;
    }
    public static String decrypt(String strToDecrypt, String secret) {
        try {
            setKey(secret);
            jpb.setValue(20);
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            jpb.setValue(40);
            return new String(cipher.doFinal(Base64.getDecoder().decode(strToDecrypt)));
        }
        catch (Exception e) {
            System.out.println("Error while decrypting: " + e.toString());
        }
        return null;
    }
    /*
     * Main
     */
	public static void main(String[] args) {
		Encryption_Decryption app = new Encryption_Decryption();	
		app.initializeWindow();
	}	
}
